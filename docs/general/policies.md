#Acceptable Use Policies and Research and Export Control Guidance

### Acceptable Use of Jetstream2

Jetstream2 requires compliance with all ACCESS and Indiana University policies, including but not limited to:

*   [ACCESS Acceptable Usage Policy](https://identity.access-ci.org/aup.html){target=_blank}
*   Indiana University's 
    *   [IT-02 Misuse and Abuse of Information Technology Resources](http://policies.iu.edu/policies/categories/information-it/it/IT-02.shtml){target=_blank}
    *   [IT-12 Security of Information Technology Resources](http://policies.iu.edu/policies/categories/information-it/it/IT-12.shtml){target=_blank}

!!! info

    Regional clouds may have additional usage and security policies. If you are using one of the regional clouds, please check with your support team there for more information.
---

### Protected Data and Jetstream2

**Jetstream2 researchers agree to:**

*   Ensure that data that must be protected by Federal security or privacy laws (e.g., HIPAA, FERPA, ITAR, classified information, export control, etc.) are **not** stored on this system unless such storage and usage is specifically authorized by the responsible University administrator and complies with any processes for management of access to such information. For export controlled information, including ITAR information, approval of the University Export Compliance Office is required prior to use of the Jetstream2 systems for storage/processing of export controlled data. The Jetstream2 system is not intended, by default, to meet the security requirements of these laws or regulations and specific usage related controls or restrictions may be required prior to authorization of the use of the Jetstream2 system for such purposes.

*   Ensure that the project does not violate any export control end use restrictions contained in [Part 744 of the EAR](https://www.bis.doc.gov/index.php/documents/regulations-docs/2343-part-744-control-policy-end-user-and-end-use-based-2/file){target=_blank}.
*   Follow all US government guidance on export controls for research and research data. Please see [Jetstream2 Export Control Guidance](export.md) for more information.

* * *

### Jetstream2 and General Research Policies

In general, fundamental, publishable research is permitted on Jetstream2 from any non-EAR sanctioned countries. Fundamental research is defined as:

> “Fundamental research means basic and applied research in science and
> engineering, the results of which ordinarily are published and shared
> broadly within the scientific community, as distinguished from proprietary
> research and from Industrial development, design, production, and product
> utilization, the results of which ordinarily are restricted for proprietary
> or national security reason.” <i>[National Security Decision Directive
> (NSDD) 189, National Policy on the Transfer of Scientific, Technical,
> and Engineering Information]</i>


For anything other than fundamental, publishable research, you may wish to
consult with your institution’s export control office for any export/sharing
restrictions.

* * *

### Specialty System (GPU & Large Memory) Policies

* Only g3.* flavors should be run on the Jetstream2-GPU resource.
* Only r3.* flavors should be run on the Jetstream2-LargeMemory resource.
* Running standard compute (m3.*) flavors on the specialty resources may result in those instances being deleted without warning.
* If GPU or large memory resources become scarce in the future, it is possible that a form of scheduling or runtime limitation may be implemented to ensure equitable access for all; however, this is not currently anticipated.

### Allocation Policies

Jetstream2 use requires an active ACCESS allocation. If your allocation expires, you will not be able to access Jetstream2 or your resources. 

Allocation PIs receive expiration notifications from ACCESS at 90, 60, and 30 days prior to expiration.

When allocations expire, Jetstream2 will do the following:

* **At the expiration point:** The allocation will be disabled on Jetstream2 and access will not be possible to allocation users.
* **10 days from expiration:** If the allocation has not been renewed *(preferred)* or extended , all instances on the allocation will be shelved and no longer accessible.
* **30 days from expiration:** If the allocation has not been renewed *(preferred)* or extended, all resources (instances, volumes, shares, images) on the allocation will be destroyed and *will not be recoverable.*
* When a [Jetstream2 Trial Allocation](../alloc/trial.md) expires, all resources (instances, volumes, shares, images) on the allocation will be destroyed **10 days after the expiration date**.

***Similar policies will apply for overdrawn allocations:***

* **When an allocation is overdrawn** (i.e., has used more SUs than it has allocated toward Jetstream2 resources), the allocation will be disabled on Jetstream2 and access will not be possible to allocation users.
* **10 days from becoming overdrawn:** If the allocation has not been renewed as a larger ACCESS project type or additional credits transferred (if available), all instances on the allocation will be shelved and no longer accessible.
* **30 days from becoming overdrawn:** If the allocation has not been renewed as a larger ACCESS project type or additional credits transferred (if available), all instances and other resources on the allocation will be destroyed and *will not be recoverable.*

### Shelved Instance Retention Policy
Instances that have been in a shelved state for over one year are considered abandoned and subject to deletion by system administrators. 

### IP (IPv4 floating IP) Policies

Floating IP numbers (also called public IPs) can be retained by an allocation for an instance under all Jetstream2 interfaces.

As long as the IP number is in regular use by instance, it may be retained. **If an instance has been shelved for 90 days or more, its public IP address is subject to reclamation.**

IP addresses are a finite resource. Any IP number associated with an instance that has been shelved for 90 days or more is considered not in use and can be reclaimed by the system without warning.

### Router Quota Policies

By default, every project on Jetstream2 is started with a **router quota = 1**, in part to maximize available IP addresses as well as decrease the time needed for periodic maintenance.

Users are advised to follow a traditional data center or building approach using one router to many subnets. The instances on a particular subnet are still isolated from the other subnet traffic.

Each project may submit a help request to increase this quota along with justification for the increase.

***There is little technical reason to have multiple routers. Routers won't be overloaded from handling many subnets***
